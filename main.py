# -- python 3.7
# -*- coding: utf-8 -*-

data_input = [7, 1, 5, 3, 6, 4]                             # dados de entrada, alterar aqui pros testes

if __name__ == '__main__':

    lucro_maximo = 0

    for i in range(0, len(data_input)):                     # percorre toda a lista de entrada
        for j in range(0, i):                               # percorre do primeiro elemento ate a posicao atual, ou seja, os valores passados
            lucro_atual = data_input[i] - data_input[j]     # calcula o lucro
            if lucro_atual > lucro_maximo:                  # esse lucro encontrado é maior?
                lucro_maximo = lucro_atual                  # armazena

    print(lucro_maximo)
